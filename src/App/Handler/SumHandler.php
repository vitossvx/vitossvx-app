<?php

declare(strict_types=1);

namespace App\Handler;

use Laminas\Diactoros\Response\JsonResponse;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;

class SumHandler implements RequestHandlerInterface
{
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $amount1 = $request->getQueryParams()['amount1'];
        $amount2 = $request->getQueryParams()['amount2'];
        $sum     = $amount1 + $amount2;
        return new JsonResponse(['sum' => $sum]);
    }
}
