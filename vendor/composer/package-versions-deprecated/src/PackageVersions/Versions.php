<?php

declare(strict_types=1);

namespace PackageVersions;

use Composer\InstalledVersions;
use OutOfBoundsException;

class_exists(InstalledVersions::class);

/**
 * This class is generated by composer/package-versions-deprecated, specifically by
 * @see \PackageVersions\Installer
 *
 * This file is overwritten at every run of `composer install` or `composer update`.
 *
 * @deprecated in favor of the Composer\InstalledVersions class provided by Composer 2. Require composer-runtime-api:^2 to ensure it is present.
 */
final class Versions
{
    /**
     * @deprecated please use {@see self::rootPackageName()} instead.
     *             This constant will be removed in version 2.0.0.
     */
    const ROOT_PACKAGE_NAME = 'mezzio/mezzio-skeleton';

    /**
     * Array of all available composer packages.
     * Dont read this array from your calling code, but use the \PackageVersions\Versions::getVersion() method instead.
     *
     * @var array<string, string>
     * @internal
     */
    const VERSIONS          = array (
  'brick/varexporter' => '0.3.5@05241f28dfcba2b51b11e2d750e296316ebbe518',
  'composer/package-versions-deprecated' => '1.11.99.2@c6522afe5540d5fc46675043d3ed5a45a740b27c',
  'fig/http-message-util' => '1.1.5@9d94dc0154230ac39e5bf89398b324a86f63f765',
  'laminas/laminas-component-installer' => '2.5.0@223d81cf648ff9380bd13cfe07a31324b0ffc8b8',
  'laminas/laminas-config-aggregator' => '1.5.0@c5908c265ada01c8952baf84f102a073de30947f',
  'laminas/laminas-diactoros' => '2.6.0@7d2034110ae18afe05050b796a3ee4b3fe177876',
  'laminas/laminas-escaper' => '2.7.0@5e04bc5ae5990b17159d79d331055e2c645e5cc5',
  'laminas/laminas-httphandlerrunner' => '1.4.0@6a2dd33e4166469ade07ad1283b45924383b224b',
  'laminas/laminas-stdlib' => '3.3.1@d81c7ffe602ed0e6ecb18691019111c0f4bf1efe',
  'laminas/laminas-stratigility' => '3.3.0@d6336b873fe8f766f84b82164f2a25e4decd6a9a',
  'laminas/laminas-zendframework-bridge' => '1.2.0@6cccbddfcfc742eb02158d6137ca5687d92cee32',
  'mezzio/mezzio' => '3.5.0@1941d51b6a29928f9da3328aba67d7bf8d9a4dc5',
  'mezzio/mezzio-helpers' => '5.6.0@d0dfb5f447faf7e019436976adec5128a0379132',
  'mezzio/mezzio-router' => '3.4.0@da7adc38450bc6db9ec583c0f3fa773f09bb02ce',
  'mezzio/mezzio-template' => '2.1.1@8f36e80b3ac6d794cf324134b368ec00bb4cfdbe',
  'nikic/php-parser' => 'v4.10.5@4432ba399e47c66624bc73c8c0f811e5c109576f',
  'psr/container' => '1.1.1@8622567409010282b7aeebe4bb841fe98b58dcaf',
  'psr/http-factory' => '1.0.1@12ac7fcd07e5b077433f5f2bee95b3a771bf61be',
  'psr/http-message' => '1.0.1@f6561bf28d520154e4b0ec72be95418abe6d9363',
  'psr/http-server-handler' => '1.0.1@aff2f80e33b7f026ec96bb42f63242dc50ffcae7',
  'psr/http-server-middleware' => '1.0.1@2296f45510945530b9dceb8bcedb5cb84d40c5f5',
  'symfony/polyfill-ctype' => 'v1.23.0@46cd95797e9df938fdd2b03693b5fca5e64b01ce',
  'webimpress/safe-writer' => '2.2.0@9d37cc8bee20f7cb2f58f6e23e05097eab5072e6',
  'webmozart/assert' => '1.10.0@6964c76c7804814a842473e0c8fd15bab0f18e25',
  'aura/di' => '4.1.0@fefda6b3db5cb04feb2bcf36f845317cfead857e',
  'aura/router' => '3.1.0@52507bc813c92511dbcacc7463f163ef5149ad38',
  'chubbyphp/chubbyphp-container' => '1.3.0@1431a5b3eda75e207e3e386a946d7081713eb0a4',
  'chubbyphp/chubbyphp-laminas-config' => '1.1.0@59b5f05e5199cd695616675ab348e8c492888a7b',
  'composer/ca-bundle' => '1.2.9@78a0e288fdcebf92aa2318a8d3656168da6ac1a5',
  'composer/composer' => '2.0.14@92b2ccbef65292ba9f2004271ef47c7231e2eed5',
  'composer/metadata-minifier' => '1.0.0@c549d23829536f0d0e984aaabbf02af91f443207',
  'composer/semver' => '3.2.5@31f3ea725711245195f62e54ffa402d8ef2fdba9',
  'composer/spdx-licenses' => '1.5.5@de30328a7af8680efdc03e396aad24befd513200',
  'composer/xdebug-handler' => '2.0.1@964adcdd3a28bf9ed5d9ac6450064e0d71ed7496',
  'container-interop/container-interop' => '1.2.0@79cbf1341c22ec75643d841642dd5d6acd83bdb8',
  'dealerdirect/phpcodesniffer-composer-installer' => 'v0.7.1@fe390591e0241955f22eb9ba327d137e501c771c',
  'doctrine/instantiator' => '1.4.0@d56bf6102915de5702778fe20f2de3b2fe570b5b',
  'elie29/zend-phpdi-config' => 'v6.0.0@887aeaaeff99f35cf48b03794ddd31db6e5ca27b',
  'filp/whoops' => '2.12.1@c13c0be93cff50f88bbd70827d993026821914dd',
  'jsoumelidis/zend-sf-di-config' => '0.4.1@2574cbe7eb3b1a8c1b34709905305dd616fa5ea3',
  'justinrainbow/json-schema' => '5.2.10@2ba9c8c862ecd5510ed16c6340aa9f6eadb4f31b',
  'laminas/laminas-auradi-config' => '2.1.0@2105edfd6a0545a692d427c933bbb65b32b6f554',
  'laminas/laminas-code' => '3.5.1@b549b70c0bb6e935d497f84f750c82653326ac77',
  'laminas/laminas-coding-standard' => '2.3.0@bcf6e07fe4690240be7beb6d884d0b0fafa6a251',
  'laminas/laminas-composer-autoloading' => '2.2.0@4267d3469df364d8375de1b675436031fd9756c4',
  'laminas/laminas-development-mode' => '3.3.0@11b2adc8837e4419a5b31e2a7ae59f06636d4096',
  'laminas/laminas-eventmanager' => '3.3.1@966c859b67867b179fde1eff0cd38df51472ce4a',
  'laminas/laminas-http' => '2.14.3@bfaab8093e382274efed7fdc3ceb15f09ba352bb',
  'laminas/laminas-json' => '3.2.0@1e3b64d3b21dac0511e628ae8debc81002d14e3c',
  'laminas/laminas-loader' => '2.7.0@bcf8a566cb9925a2e7cc41a16db09235ec9fb616',
  'laminas/laminas-pimple-config' => '1.1.2@87eb235eb37ebc5ee4c40d43b6c605657dd3fad6',
  'laminas/laminas-psr7bridge' => '1.3.1@b1158f0574164039f698a261c01c26119cc6dbd6',
  'laminas/laminas-router' => '3.4.5@aaf2eb364eedeb5c4d5b9ee14cd2938d0f7e89b7',
  'laminas/laminas-servicemanager' => '3.6.4@b1445e1a7077c21b0fad0974a1b7a11b9dbe0828',
  'laminas/laminas-uri' => '2.8.1@79bd4c614c8cf9a6ba715a49fca8061e84933d87',
  'laminas/laminas-validator' => '2.14.4@e370c4695db1c81e6dfad38d8c4dbdb37b23d776',
  'laminas/laminas-view' => '2.12.0@3ef103da6887809f08ecf52f42c31a76c9bf08b1',
  'league/plates' => 'v3.4.0@6d3ee31199b536a4e003b34a356ca20f6f75496a',
  'mezzio/mezzio-aurarouter' => '3.1.0@3dc711c9408e2c32c94a446d8360043e955d99cc',
  'mezzio/mezzio-fastroute' => '3.2.0@faa586b35dc76231a5dce736227046ccbee2b5f1',
  'mezzio/mezzio-laminasrouter' => '3.1.0@1720c3bb24d40ff00b7bdc0d4feb3d375921daa5',
  'mezzio/mezzio-laminasviewrenderer' => '2.3.0@6f710b5a4cac2e457aa1adc81e24197ae6e2b3d8',
  'mezzio/mezzio-platesrenderer' => '2.3.0@d2792f73e2a4d52682e7ef2eb5065026104039f3',
  'mezzio/mezzio-tooling' => '1.4.0@2ad3390459183ee2c670bcb507ce334bcc219e23',
  'mezzio/mezzio-twigrenderer' => '2.7.0@ce4ba692548d5de26038453c4202bf1b7934bd06',
  'mikey179/vfsstream' => 'v1.6.8@231c73783ebb7dd9ec77916c10037eff5a2b6efe',
  'myclabs/deep-copy' => '1.10.2@776f831124e9c62e1a2c601ecc52e776d8bb7220',
  'nikic/fast-route' => 'v1.3.0@181d480e08d9476e61381e04a71b34dc0432e812',
  'northwoods/container' => '3.2.0@52a9db4450d7a7c883290d6c4990116185a9b6e3',
  'opis/closure' => '3.6.2@06e2ebd25f2869e54a306dda991f7db58066f7f6',
  'phar-io/manifest' => '2.0.1@85265efd3af7ba3ca4b2a2c34dbfc5788dd29133',
  'phar-io/version' => '3.1.0@bae7c545bef187884426f042434e561ab1ddb182',
  'php-di/invoker' => '2.3.0@992fec6c56f2d1ad1ad5fee28267867c85bfb8f9',
  'php-di/php-di' => '6.3.3@da8e476cafc8011477e2ec9fd2e4706947758af2',
  'php-di/phpdoc-reader' => '2.2.1@66daff34cbd2627740ffec9469ffbac9f8c8185c',
  'phpdocumentor/reflection-common' => '2.2.0@1d01c49d4ed62f25aa84a747ad35d5a16924662b',
  'phpdocumentor/reflection-docblock' => '5.2.2@069a785b2141f5bcf49f3e353548dc1cce6df556',
  'phpdocumentor/type-resolver' => '1.4.0@6a467b8989322d92aa1c8bf2bebcc6e5c2ba55c0',
  'phpspec/prophecy' => '1.13.0@be1996ed8adc35c3fd795488a653f4b518be70ea',
  'phpspec/prophecy-phpunit' => 'v2.0.1@2d7a9df55f257d2cba9b1d0c0963a54960657177',
  'phpstan/phpdoc-parser' => '0.5.4@e352d065af1ae9b41c12d1dfd309e90f7b1f55c9',
  'phpstan/phpstan' => '0.12.88@464d1a81af49409c41074aa6640ed0c4cbd9bb68',
  'phpstan/phpstan-strict-rules' => '0.12.9@0705fefc7c9168529fd130e341428f5f10f4f01d',
  'phpunit/php-code-coverage' => '9.2.6@f6293e1b30a2354e8428e004689671b83871edde',
  'phpunit/php-file-iterator' => '3.0.5@aa4be8575f26070b100fccb67faabb28f21f66f8',
  'phpunit/php-invoker' => '3.1.1@5a10147d0aaf65b58940a0b72f71c9ac0423cc67',
  'phpunit/php-text-template' => '2.0.4@5da5f67fc95621df9ff4c4e5a84d6a8a2acf7c28',
  'phpunit/php-timer' => '5.0.3@5a63ce20ed1b5bf577850e2c4e87f4aa902afbd2',
  'phpunit/phpunit' => '9.5.4@c73c6737305e779771147af66c96ca6a7ed8a741',
  'pimple/pimple' => 'v3.4.0@86406047271859ffc13424a048541f4531f53601',
  'psr/log' => '1.1.4@d49695b909c3b7628b6289db5479a1c204601f11',
  'rdlowrey/auryn' => 'v1.4.4@dae57592229d313b59414a2c8334e61e6eb00928',
  'react/promise' => 'v2.8.0@f3cff96a19736714524ca0dd1d4130de73dbbbc4',
  'roave/security-advisories' => 'dev-master@80f32ef3cff7c3b5b650d8aa823437b43d5c5056',
  'sebastian/cli-parser' => '1.0.1@442e7c7e687e42adc03470c7b668bc4b2402c0b2',
  'sebastian/code-unit' => '1.0.8@1fc9f64c0927627ef78ba436c9b17d967e68e120',
  'sebastian/code-unit-reverse-lookup' => '2.0.3@ac91f01ccec49fb77bdc6fd1e548bc70f7faa3e5',
  'sebastian/comparator' => '4.0.6@55f4261989e546dc112258c7a75935a81a7ce382',
  'sebastian/complexity' => '2.0.2@739b35e53379900cc9ac327b2147867b8b6efd88',
  'sebastian/diff' => '4.0.4@3461e3fccc7cfdfc2720be910d3bd73c69be590d',
  'sebastian/environment' => '5.1.3@388b6ced16caa751030f6a69e588299fa09200ac',
  'sebastian/exporter' => '4.0.3@d89cc98761b8cb5a1a235a6b703ae50d34080e65',
  'sebastian/global-state' => '5.0.2@a90ccbddffa067b51f574dea6eb25d5680839455',
  'sebastian/lines-of-code' => '1.0.3@c1c2e997aa3146983ed888ad08b15470a2e22ecc',
  'sebastian/object-enumerator' => '4.0.4@5c9eeac41b290a3712d88851518825ad78f45c71',
  'sebastian/object-reflector' => '2.0.4@b4f479ebdbf63ac605d183ece17d8d7fe49c15c7',
  'sebastian/recursion-context' => '4.0.4@cd9d8cf3c5804de4341c283ed787f099f5506172',
  'sebastian/resource-operations' => '3.0.3@0f4443cb3a1d92ce809899753bc0d5d5a8dd19a8',
  'sebastian/type' => '2.3.1@81cd61ab7bbf2de744aba0ea61fae32f721df3d2',
  'sebastian/version' => '3.0.2@c6c1022351a901512170118436c764e473f6de8c',
  'seld/jsonlint' => '1.8.3@9ad6ce79c342fbd44df10ea95511a1b24dee5b57',
  'seld/phar-utils' => '1.1.1@8674b1d84ffb47cc59a101f5d5a3b61e87d23796',
  'slevomat/coding-standard' => '7.0.8@48141737f9e5ed701ef8bcea2027e701b50bd932',
  'squizlabs/php_codesniffer' => '3.6.0@ffced0d2c8fa8e6cdc4d695a743271fab6c38625',
  'symfony/console' => 'v5.2.8@864568fdc0208b3eba3638b6000b69d2386e6768',
  'symfony/dependency-injection' => 'v5.2.9@2761ca2f7e2f41af3a45951e1ce8c01f121245eb',
  'symfony/deprecation-contracts' => 'v2.4.0@5f38c8804a9e97d23e0c8d63341088cd8a22d627',
  'symfony/filesystem' => 'v5.2.7@056e92acc21d977c37e6ea8e97374b2a6c8551b0',
  'symfony/finder' => 'v5.2.9@ccccb9d48ca42757dd12f2ca4bf857a4e217d90d',
  'symfony/polyfill-intl-grapheme' => 'v1.23.0@24b72c6baa32c746a4d0840147c9715e42bb68ab',
  'symfony/polyfill-intl-normalizer' => 'v1.23.0@8590a5f561694770bdcd3f9b5c69dde6945028e8',
  'symfony/polyfill-mbstring' => 'v1.23.0@2df51500adbaebdc4c38dea4c89a2e131c45c8a1',
  'symfony/polyfill-php73' => 'v1.23.0@fba8933c384d6476ab14fb7b8526e5287ca7e010',
  'symfony/polyfill-php80' => 'v1.23.0@eca0bf41ed421bed1b57c4958bab16aa86b757d0',
  'symfony/process' => 'v5.2.7@98cb8eeb72e55d4196dd1e36f1f16e7b3a9a088e',
  'symfony/service-contracts' => 'v2.4.0@f040a30e04b57fbcc9c6cbcf4dbaa96bd318b9bb',
  'symfony/string' => 'v5.2.8@01b35eb64cac8467c3f94cd0ce2d0d376bb7d1db',
  'theseer/tokenizer' => '1.2.0@75a63c33a8577608444246075ea0af0d052e452a',
  'twig/twig' => 'v3.3.2@21578f00e83d4a82ecfa3d50752b609f13de6790',
  'webimpress/coding-standard' => '1.2.2@8f4a220de33f471a8101836f7ec72b852c3f9f03',
  'mezzio/mezzio-skeleton' => '1.0.0+no-version-set@',
);

    private function __construct()
    {
    }

    /**
     * @psalm-pure
     *
     * @psalm-suppress ImpureMethodCall we know that {@see InstalledVersions} interaction does not
     *                                  cause any side effects here.
     */
    public static function rootPackageName() : string
    {
        if (!class_exists(InstalledVersions::class, false) || !(method_exists(InstalledVersions::class, 'getAllRawData') ? InstalledVersions::getAllRawData() : InstalledVersions::getRawData())) {
            return self::ROOT_PACKAGE_NAME;
        }

        return InstalledVersions::getRootPackage()['name'];
    }

    /**
     * @throws OutOfBoundsException If a version cannot be located.
     *
     * @psalm-param key-of<self::VERSIONS> $packageName
     * @psalm-pure
     *
     * @psalm-suppress ImpureMethodCall we know that {@see InstalledVersions} interaction does not
     *                                  cause any side effects here.
     */
    public static function getVersion(string $packageName): string
    {
        if (class_exists(InstalledVersions::class, false) && (method_exists(InstalledVersions::class, 'getAllRawData') ? InstalledVersions::getAllRawData() : InstalledVersions::getRawData())) {
            return InstalledVersions::getPrettyVersion($packageName)
                . '@' . InstalledVersions::getReference($packageName);
        }

        if (isset(self::VERSIONS[$packageName])) {
            return self::VERSIONS[$packageName];
        }

        throw new OutOfBoundsException(
            'Required package "' . $packageName . '" is not installed: check your ./vendor/composer/installed.json and/or ./composer.lock files'
        );
    }
}
