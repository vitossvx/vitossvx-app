<?php

// autoload_psr4.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'phpDocumentor\\Reflection\\' => array($vendorDir . '/phpdocumentor/reflection-common/src', $vendorDir . '/phpdocumentor/reflection-docblock/src', $vendorDir . '/phpdocumentor/type-resolver/src'),
    'Whoops\\' => array($vendorDir . '/filp/whoops/src/Whoops'),
    'Webmozart\\Assert\\' => array($vendorDir . '/webmozart/assert/src'),
    'Webimpress\\SafeWriter\\' => array($vendorDir . '/webimpress/safe-writer/src'),
    'WebimpressCodingStandard\\' => array($vendorDir . '/webimpress/coding-standard/src/WebimpressCodingStandard'),
    'Twig\\' => array($vendorDir . '/twig/twig/src'),
    'Symfony\\Polyfill\\Php80\\' => array($vendorDir . '/symfony/polyfill-php80'),
    'Symfony\\Polyfill\\Php73\\' => array($vendorDir . '/symfony/polyfill-php73'),
    'Symfony\\Polyfill\\Mbstring\\' => array($vendorDir . '/symfony/polyfill-mbstring'),
    'Symfony\\Polyfill\\Intl\\Normalizer\\' => array($vendorDir . '/symfony/polyfill-intl-normalizer'),
    'Symfony\\Polyfill\\Intl\\Grapheme\\' => array($vendorDir . '/symfony/polyfill-intl-grapheme'),
    'Symfony\\Polyfill\\Ctype\\' => array($vendorDir . '/symfony/polyfill-ctype'),
    'Symfony\\Contracts\\Service\\' => array($vendorDir . '/symfony/service-contracts'),
    'Symfony\\Component\\String\\' => array($vendorDir . '/symfony/string'),
    'Symfony\\Component\\Process\\' => array($vendorDir . '/symfony/process'),
    'Symfony\\Component\\Finder\\' => array($vendorDir . '/symfony/finder'),
    'Symfony\\Component\\Filesystem\\' => array($vendorDir . '/symfony/filesystem'),
    'Symfony\\Component\\DependencyInjection\\' => array($vendorDir . '/symfony/dependency-injection'),
    'Symfony\\Component\\Console\\' => array($vendorDir . '/symfony/console'),
    'SlevomatCodingStandard\\' => array($vendorDir . '/slevomat/coding-standard/SlevomatCodingStandard'),
    'Seld\\PharUtils\\' => array($vendorDir . '/seld/phar-utils/src'),
    'Seld\\JsonLint\\' => array($vendorDir . '/seld/jsonlint/src/Seld/JsonLint'),
    'React\\Promise\\' => array($vendorDir . '/react/promise/src'),
    'Psr\\Log\\' => array($vendorDir . '/psr/log/Psr/Log'),
    'Psr\\Http\\Server\\' => array($vendorDir . '/psr/http-server-handler/src', $vendorDir . '/psr/http-server-middleware/src'),
    'Psr\\Http\\Message\\' => array($vendorDir . '/psr/http-factory/src', $vendorDir . '/psr/http-message/src'),
    'Psr\\Container\\' => array($vendorDir . '/psr/container/src'),
    'Prophecy\\PhpUnit\\' => array($vendorDir . '/phpspec/prophecy-phpunit/src'),
    'Prophecy\\' => array($vendorDir . '/phpspec/prophecy/src/Prophecy'),
    'PhpParser\\' => array($vendorDir . '/nikic/php-parser/lib/PhpParser'),
    'PhpDocReader\\' => array($vendorDir . '/php-di/phpdoc-reader/src/PhpDocReader'),
    'PackageVersions\\' => array($vendorDir . '/composer/package-versions-deprecated/src/PackageVersions'),
    'PHPStan\\PhpDocParser\\' => array($vendorDir . '/phpstan/phpdoc-parser/src'),
    'PHPStan\\' => array($vendorDir . '/phpstan/phpstan-strict-rules/src'),
    'Opis\\Closure\\' => array($vendorDir . '/opis/closure/src'),
    'Northwoods\\Container\\' => array($vendorDir . '/northwoods/container/src'),
    'Mezzio\\Twig\\' => array($vendorDir . '/mezzio/mezzio-twigrenderer/src'),
    'Mezzio\\Tooling\\' => array($vendorDir . '/mezzio/mezzio-tooling/src'),
    'Mezzio\\Template\\' => array($vendorDir . '/mezzio/mezzio-template/src'),
    'Mezzio\\Router\\' => array($vendorDir . '/mezzio/mezzio-aurarouter/src', $vendorDir . '/mezzio/mezzio-fastroute/src', $vendorDir . '/mezzio/mezzio-laminasrouter/src', $vendorDir . '/mezzio/mezzio-router/src'),
    'Mezzio\\Plates\\' => array($vendorDir . '/mezzio/mezzio-platesrenderer/src'),
    'Mezzio\\LaminasView\\' => array($vendorDir . '/mezzio/mezzio-laminasviewrenderer/src'),
    'Mezzio\\Helper\\' => array($vendorDir . '/mezzio/mezzio-helpers/src'),
    'Mezzio\\' => array($vendorDir . '/mezzio/mezzio/src'),
    'League\\Plates\\' => array($vendorDir . '/league/plates/src'),
    'Laminas\\ZendFrameworkBridge\\' => array($vendorDir . '/laminas/laminas-zendframework-bridge/src'),
    'Laminas\\View\\' => array($vendorDir . '/laminas/laminas-view/src'),
    'Laminas\\Validator\\' => array($vendorDir . '/laminas/laminas-validator/src'),
    'Laminas\\Uri\\' => array($vendorDir . '/laminas/laminas-uri/src'),
    'Laminas\\Stratigility\\' => array($vendorDir . '/laminas/laminas-stratigility/src'),
    'Laminas\\Stdlib\\' => array($vendorDir . '/laminas/laminas-stdlib/src'),
    'Laminas\\ServiceManager\\' => array($vendorDir . '/laminas/laminas-servicemanager/src'),
    'Laminas\\Router\\' => array($vendorDir . '/laminas/laminas-router/src'),
    'Laminas\\Psr7Bridge\\' => array($vendorDir . '/laminas/laminas-psr7bridge/src'),
    'Laminas\\Pimple\\Config\\' => array($vendorDir . '/laminas/laminas-pimple-config/src'),
    'Laminas\\Loader\\' => array($vendorDir . '/laminas/laminas-loader/src'),
    'Laminas\\Json\\' => array($vendorDir . '/laminas/laminas-json/src'),
    'Laminas\\Http\\' => array($vendorDir . '/laminas/laminas-http/src'),
    'Laminas\\HttpHandlerRunner\\' => array($vendorDir . '/laminas/laminas-httphandlerrunner/src'),
    'Laminas\\EventManager\\' => array($vendorDir . '/laminas/laminas-eventmanager/src'),
    'Laminas\\Escaper\\' => array($vendorDir . '/laminas/laminas-escaper/src'),
    'Laminas\\Diactoros\\' => array($vendorDir . '/laminas/laminas-diactoros/src'),
    'Laminas\\DevelopmentMode\\' => array($vendorDir . '/laminas/laminas-development-mode/src'),
    'Laminas\\ConfigAggregator\\' => array($vendorDir . '/laminas/laminas-config-aggregator/src'),
    'Laminas\\ComposerAutoloading\\' => array($vendorDir . '/laminas/laminas-composer-autoloading/src'),
    'Laminas\\ComponentInstaller\\' => array($vendorDir . '/laminas/laminas-component-installer/src'),
    'Laminas\\Code\\' => array($vendorDir . '/laminas/laminas-code/src'),
    'Laminas\\AuraDi\\Config\\' => array($vendorDir . '/laminas/laminas-auradi-config/src'),
    'LaminasCodingStandard\\' => array($vendorDir . '/laminas/laminas-coding-standard/src/LaminasCodingStandard'),
    'JsonSchema\\' => array($vendorDir . '/justinrainbow/json-schema/src/JsonSchema'),
    'JSoumelidis\\SymfonyDI\\Config\\' => array($vendorDir . '/jsoumelidis/zend-sf-di-config/src'),
    'Invoker\\' => array($vendorDir . '/php-di/invoker/src'),
    'Interop\\Container\\' => array($vendorDir . '/container-interop/container-interop/src/Interop/Container'),
    'Fig\\Http\\Message\\' => array($vendorDir . '/fig/http-message-util/src'),
    'FastRoute\\' => array($vendorDir . '/nikic/fast-route/src'),
    'Elie\\PHPDI\\Config\\' => array($vendorDir . '/elie29/zend-phpdi-config/src'),
    'Doctrine\\Instantiator\\' => array($vendorDir . '/doctrine/instantiator/src/Doctrine/Instantiator'),
    'DeepCopy\\' => array($vendorDir . '/myclabs/deep-copy/src/DeepCopy'),
    'Dealerdirect\\Composer\\Plugin\\Installers\\PHPCodeSniffer\\' => array($vendorDir . '/dealerdirect/phpcodesniffer-composer-installer/src'),
    'DI\\' => array($vendorDir . '/php-di/php-di/src'),
    'Composer\\XdebugHandler\\' => array($vendorDir . '/composer/xdebug-handler/src'),
    'Composer\\Spdx\\' => array($vendorDir . '/composer/spdx-licenses/src'),
    'Composer\\Semver\\' => array($vendorDir . '/composer/semver/src'),
    'Composer\\MetadataMinifier\\' => array($vendorDir . '/composer/metadata-minifier/src'),
    'Composer\\CaBundle\\' => array($vendorDir . '/composer/ca-bundle/src'),
    'Composer\\' => array($vendorDir . '/composer/composer/src/Composer'),
    'Chubbyphp\\Laminas\\Config\\' => array($vendorDir . '/chubbyphp/chubbyphp-laminas-config/src'),
    'Chubbyphp\\Container\\' => array($vendorDir . '/chubbyphp/chubbyphp-container/src'),
    'Brick\\VarExporter\\' => array($vendorDir . '/brick/varexporter/src'),
    'Auryn\\' => array($vendorDir . '/rdlowrey/auryn/lib'),
    'Aura\\Router\\' => array($vendorDir . '/aura/router/src'),
    'Aura\\Di\\' => array($vendorDir . '/aura/di/src'),
    'App\\' => array($baseDir . '/src/App'),
    'AppTest\\' => array($baseDir . '/test/AppTest'),
);
